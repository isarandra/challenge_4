package com.example.application.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Film {

    @Id
    @SequenceGenerator(
            name = "film_code_sequence",
            sequenceName = "film_code_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "film_code_sequence"
    )
    private Long filmCode;
    private String filmName;
    private boolean sedangTayang;

    @OneToMany(
            targetEntity = Schedule.class,
            mappedBy = "film",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
//    @JoinColumn(
//            name = "schedule_id",
//            referencedColumnName = "scheduleId"
//    )
    private List<Schedule> schedule;
}

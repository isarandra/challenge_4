package com.example.application.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SeatId implements Serializable {

    private String studioName;

    private String nomorKursi;

    private Long scheduleId;
}

package com.example.application.repository;

import com.example.application.entity.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmRepository extends JpaRepository<Film, Long> {

    @Query(value = "select * from film where film_name = ?1",nativeQuery = true)
    List<Film> findFilmByFilmName(@Param("film_name") String film_name);

    @Query(value = "select * from film where film_code = ?1",nativeQuery = true)
    List<Film> findFilmByFilmCode(@Param("film_code") Integer film_code);

    @Query(value="select * from film where sedang_tayang = true",nativeQuery = true)
    List<Film> findAllFilmYangSedangTayang();

    @Query(value="select film_code from film where film_name=:filmName",nativeQuery = true)
    Long getFilmCode(@Param("filmName") String filmName);

    @Query(value="call change_film_name(:filmNameOld,:filmNameNew)",nativeQuery = true)
    void changeFilmName(String filmNameOld, String filmNameNew);

    @Query(value = "delete from film where film_name=:film_name",nativeQuery = true)
    void deleteFilm(@Param("film_name") String film_name);

//    @Query(value = "update film" +
//            " set film_name = :film_name_new" +
//            " where film_name = :film_name_old",nativeQuery = true)
//    void changeFilmName(@Param("film_name_old") String film_name_old,@Param("film_name_new") String film_name_new);
}

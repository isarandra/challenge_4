package com.example.application.service;

import com.example.application.entity.ActiveUser;
import com.example.application.entity.Film;
import com.example.application.repository.ActiveUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActiveUserService implements IActiveUserService{

    @Autowired
    ActiveUserRepository activeUserRepository;

    @Override
    public void newActiveUser(ActiveUser activeUser) throws Exception {
        List<ActiveUser> listActiveUser = activeUserRepository.findActiveUserByUsername(activeUser.getUsername());
        if(listActiveUser.size()>0){
            throw new Exception("User sudah terdaftar");
        }
        activeUserRepository.save(activeUser);
    }

//    @Override
//    public void updateActiveUser(String usernameOld, String usernameNew,
//                                 String emailAddressNew, String passwordNew) throws Exception {
//        activeUserRepository.updateActiveUser(usernameOld,usernameNew,emailAddressNew,passwordNew);
//    }

    @Override
    public void deleteActiveUser(String username) throws Exception {
        List<ActiveUser> listActiveUser = activeUserRepository.findActiveUserByUsername(username);
        if(listActiveUser.size()==0){
            throw new Exception("Username tidak ditemukan");
        }
        activeUserRepository.deleteActiveUser(username);
    }
}

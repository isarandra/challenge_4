package com.example.application.service;

import com.example.application.entity.ActiveUser;
import org.springframework.stereotype.Service;

@Service
public interface IActiveUserService {

    void newActiveUser(ActiveUser activeUser) throws Exception;

//    void updateActiveUser(String usernameOld, String usernameNew,
//                          String emailAddressNew,
//                          String passwordNew) throws Exception;

    void deleteActiveUser(String username) throws Exception;
}

package com.example.application.service;

import com.example.application.entity.ActiveUser;
import com.example.application.entity.Film;
import org.springframework.stereotype.Service;

@Service
public interface IFilmService {

    void newFilm(Film film) throws Exception;

//    void changeFilmName(String filmNameOld, String filmNameNew) throws Exception;

    void updateNameFilm(String namaFilmLama, String namaFilmBaru) throws Exception;
    void deleteFilm(String filmName) throws Exception;
    void printFilmYangSedangTayang() throws Exception;

}

package com.example.application.service;

import com.example.application.entity.Schedule;
import org.springframework.stereotype.Service;

@Service
public interface IScheduleService {

    void newSchedule(Schedule schedule);
}

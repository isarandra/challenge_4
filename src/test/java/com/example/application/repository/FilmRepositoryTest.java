package com.example.application.repository;

import com.example.application.entity.Film;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class FilmRepositoryTest {

    @Autowired
    FilmRepository filmRepository;

    @Test
    void findFilmByFilmName(){
        List<Film> listFilm = filmRepository.findFilmByFilmName("One Piece");
        listFilm.forEach(film -> {
            System.out.println(film.getFilmName());
        });
    }

    @Test
    void findFilmByFilmCode(){
        List<Film> listFilm = filmRepository.findFilmByFilmCode(3);
        listFilm.forEach(film -> {
            System.out.println(film.getFilmName());
        });
    }

    @Test
    void insertFilm(){
        Film film = new Film();
        film.setFilmName("Two Piece");
        film.setSedangTayang(true);

        filmRepository.save(film);
    }

    @Test
    void changeFilmName(){
        Film film = new Film();
        film.setFilmName("Two Piece");
        filmRepository.save(film);
        try{
            filmRepository.changeFilmName("Two Piece","Interstellar");
        }
        catch (Exception e){

        }
    }

    @Test
    void deleteFilm(){
        try{
            filmRepository.deleteFilm("One Piece");
        }
        catch (Exception e){

        }
    }

    @Test
    void findAllFilmYangSedangTayang(){
        Film film = new Film();
        film.setFilmName("One Piece");
        film.setSedangTayang(true);
        filmRepository.save(film);

        Film film1 = new Film();
        film1.setFilmName("Two Piece");
        film1.setSedangTayang(false);
        filmRepository.save(film1);

        Film film2 = new Film();
        film2.setFilmName("Three Piece");
        film2.setSedangTayang(true);
        filmRepository.save(film2);

        List<Film> listFilm = filmRepository.findAllFilmYangSedangTayang();
        System.out.print("Film yang sedang tayang: ");
        listFilm.forEach(filmm -> {
            System.out.print(filmm.getFilmName()+" ");
        });
        System.out.println();
    }

    @Test
    void getFilmCode(){
        Film film = new Film();
        film.setFilmName("Game of Thrones");
        film.setSedangTayang(true);
        filmRepository.save(film);
        Long filmCode=0L;
        try{
            filmCode =filmRepository.getFilmCode("Game of Thrones");
        }
        catch (Exception e){

        }
        System.out.println(filmCode);
    }
}

package com.example.application.repository;

import com.example.application.entity.Film;
import com.example.application.entity.Schedule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class ScheduleRepositoryTest {

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    FilmRepository filmRepository;

    @Test
    public void saveScheduleWithFilm(){
        Film film = Film
                .builder()
                .filmName("Money Heist")
                .sedangTayang(true)
                .build();

        Schedule schedule = Schedule
                .builder()
                .film(film)
                .hargaTiket(45000L)
                .jamMulai("13.00 WIB")
                .jamSelesai("16.00 WIB")
                .tanggalTayang("7 Nov 2022")
                .studioName("C")
                .build();

        scheduleRepository.save(schedule);
    }

}

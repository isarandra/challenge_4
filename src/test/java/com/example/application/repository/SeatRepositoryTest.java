package com.example.application.repository;

import com.example.application.entity.Seat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SeatRepositoryTest {

    @Autowired
    SeatRepository seatRepository;

    @Autowired
    ScheduleRepository scheduleRepository;

    @Test
    public void insertSeat(){

        Long scheduleid=36l;
        String studioName="B";

        Seat seatA1 = Seat
                .builder()
                .studioName(studioName)
                .nomorKursi(studioName+"1")
                .scheduleId(scheduleid)
                .build();

        seatRepository.save(seatA1);

        Seat seatA2 = Seat
                .builder()
                .studioName(studioName)
                .nomorKursi(studioName+"2")
                .scheduleId(scheduleid)
                .build();

        seatRepository.save(seatA2);

        Seat seatA3 = Seat
                .builder()
                .studioName(studioName)
                .nomorKursi(studioName+"3")
                .scheduleId(scheduleid)
                .build();

        seatRepository.save(seatA3);
    }

    @Test
    public void getAllAvailableSeatByScheduleIdAndStudioName(){
        List<Seat> seatList= seatRepository.getAllAvailableSeatByScheduleIdAndStudioName(15l,"A");

        System.out.print("Available seat: ");
        seatList.forEach(sl->{
            System.out.print(sl.getNomorKursi()+" ");
        });
        System.out.println();
    }

    @Test
    public void deleteRowByScheduleIdAndNomorKursi(){
        try{
            seatRepository.deleteRowByScheduleIdAndNomorKursi(15l,"A2");
        }
        catch (Exception e){

        }

        List<Seat> seatList= seatRepository.getAllAvailableSeatByScheduleIdAndStudioName(15l,"A");

        System.out.print("Available seat: ");
        seatList.forEach(sl->{
            System.out.print(sl.getNomorKursi()+" ");
        });
        System.out.println();
    }
}

package com.example.application.service;

import com.example.application.entity.ActiveUser;
import com.example.application.entity.Film;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ActiveUserServiceTest {

    @Autowired
    IActiveUserService activeUserService;

    @Test
    void newActiveUser(){
        ActiveUser activeUser=ActiveUser.builder().username("isa keong")
                .emailAddress("isakeong@yahoo.com").password("isakeong").build();
        try{
            activeUserService.newActiveUser(activeUser);
        }
        catch (Exception e){

        }
    }

    @Test
    void newActiveUser_alreadyRegistered(){
        ActiveUser activeUser = new ActiveUser();
        activeUser.setUsername("Uchiha Sasuge");
        try{
            activeUserService.newActiveUser(activeUser);
        }
        catch (Exception e){

        }
        Assertions.assertThrows(Exception.class, () -> activeUserService.newActiveUser(activeUser));
    }

    @Test
    void deleteActiveUser(){
        ActiveUser activeUser = new ActiveUser();
        activeUser.setUsername("Hatake Kakadi");
        try{
            activeUserService.newActiveUser(activeUser);
        }
        catch (Exception e){

        }
        try{
            activeUserService.deleteActiveUser("Hatake Kakadi");
        }
        catch (Exception e){

        }
    }

//    @Test
//    void updateActiveUser(){
//        ActiveUser activeUser = new ActiveUser();
//        activeUser.setUsername("Uzumaki Naruto");
//        try{
//            activeUserService.newActiveUser(activeUser);
//        }
//        catch (Exception e){
//
//        }
//        try{
//            activeUserService.updateActiveUser("Uzumaki Naruto", "Uzumaki Naruho",
//                    "uzumakinaruho@yuhaa.com",
//                    "123321");
//        }
//        catch(Exception e){
//
//        }
//    }
}

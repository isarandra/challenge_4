package com.example.application.service;

import com.example.application.entity.Film;
import com.example.application.entity.Schedule;
import com.example.application.repository.FilmRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ScheduleServiceTest {
    @Autowired
    ScheduleService scheduleService;
    @Autowired
    FilmRepository filmRepository;

    @Test
    void newSchedule(){

        Film film = Film.builder().filmName("Interstellar").sedangTayang(true).build();
//        Film film = filmRepository.findFilmByFilmName("Naruto").get(0);
        Schedule schedule=Schedule.builder()
                .film(film)
                .hargaTiket(60000L)
                .tanggalTayang("8 Nov 2022")
                .jamMulai("20.00 WIB")
                .jamSelesai("22.30 WIB")
                .studioName("B").build();
        try{
            scheduleService.newSchedule(schedule);
        }
        catch (Exception e){

        }
    }
}
